from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import JsonResponse
from .encoders import (
 SalespersonEncoder, CustomerEncoder, SaleListEncoder, AutomobileVOEncoder
)
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
import json


# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_salesmen(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        print (content)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesman(request, salesperson_id):
    salesperson = Salesperson.objects.get(id=salesperson_id)
    if request.method == "DELETE":
        salesperson.delete()
        return JsonResponse(
            {"message": "Salesperson deleted"},
            encoder=SalespersonEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        print(content)
        salesperson.first_name = content["first_name"]
        salesperson.last_name = content["last_name"]
        salesperson.employee_id = content["employee_id"]
        salesperson.save()
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, customer_id):
    customer = Customer.objects.get(id=customer_id)
    if request.method == "DELETE":
        customer.delete()
        return JsonResponse(
            {"message": "Customer deleted"},
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        print(content)
        customer.first_name = content["first_name"]
        customer.last_name = content["last_name"]
        customer.address = content["address"]
        customer.phone_number = content["phone_number"]
        customer.save()
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            sales = Sale.objects.filter(automobile=automobile_vo_id)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid automobile href"},
                status=400,
            )

        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid salesperson"}
            )
            response.status_code = 400
            return response

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid customer"}
            )
            response.status_code = 400
            return response

        sale = Sale.objects.create(**content)
        return JsonResponse(
            {"sale": sale},
            encoder=SaleListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_sale(request, sale_id):
    sale = Sale.objects.get(id=sale_id)
    if request.method == "DELETE":
        sale.delete()
        return JsonResponse(
            {"message": "Sale deleted"},
            encoder=SaleListEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        print(content)
        sale.price = content["price"]
        sale.save()
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )
