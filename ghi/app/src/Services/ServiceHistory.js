import React, { useEffect, useState } from 'react';
function ServiceHistory() {
const [appointments, setAppointments] = useState();
const [automobiles, setAutomobiles] = useState();
const [technicians, setTechnicians] = useState();
const [error, setError] = useState();
const [filteredValue, setFilteredValue] = useState();
const [filteredAutomobiles, setFilteredAutomobiles] = useState([]);

const getTechnitionData = async () => {
    // gets database information from docker server
    const techniciansurl = 'http://localhost:8080/api/technicians/';
    const response = await fetch(techniciansurl);

    if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
    }
}  
  
    useEffect(() => {
        getTechnitionData().then(data => setTechnicians(data));
        getTechnitionData();
    }, []);

const getAutomobilesData = async () => {
    try {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        } else {
            setError('Failed to fetch automobiles data');
        }
    } catch (error) {
        setError('An error occurred while fetching automobiles data');
    }
};
useEffect(() => {
    getAutomobilesData();
}, []);

const getAppointmentsData = async () => {
    try {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        } else {
            setError('Failed to fetch appointments data');
        }
    } catch (error) {
        setError('An error occurred while fetching appointments data');
    }
};
useEffect(() => {
    getAppointmentsData();
}, []);
async function handleVip() {
    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const automobileResponse = await fetch(automobileUrl);
    const appointmentResponse = await fetch(appointmentUrl);
    if (automobileResponse.ok && appointmentResponse.ok) {
        const automobileData = await automobileResponse.json();
        const appointmentData = await appointmentResponse.json();
        const filteredAutomobiles = automobileData.autos.filter(automobile => {
            return appointmentData.appointments.some(appointment => appointment.vin === automobile.vin);
        });
        setFilteredAutomobiles(filteredAutomobiles);
    }
}
useEffect(() => {
    handleVip();
}, []);

function handleFilterChange(event) {
    console.log(event.target.value);
    setFilteredValue(event.target.value)
}
return (
    <div>
        <h1>Service History</h1>
        <div style={{display: 'flex'}}>
        <input style={{width: '200px'}} onChange={handleFilterChange} placeholder="Search by VIN" required type="text" name="filteredData" id="filteredData" className="form-control" />
              <label htmlFor="filteredData"></label>
        </div>
        <div>
            <h3>Search Results</h3>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer Name</th>
                        <th>Date Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {appointments && appointments
                .filter((appointment) => appointment.vin.toLowerCase().includes(filteredValue))
                .map((appointment )=> {
                    const technician = technicians && technicians.find(tech => tech.id === appointment.technician_id);

                    const date = new Date(appointment.date_time);
                    const formattedDate = date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
                    const formattedTime = date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0');

                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>
                                        {filteredAutomobiles.some(
                                            (automobile) => automobile.vin === appointment.vin
                                        )
                                            ? 'Yes'
                                            : 'No'}
                                    </td>
                            <td>{appointment.customer}</td>
                            <td>{formattedDate + ' ' + formattedTime}</td>
                            <td>{technicians ? `${technician.first_name} ${technician.last_name}` : 'N/A'}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
        <h2>All Appointments</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>VIP</th>
                    <th>Customer Name</th>
                    <th>Date Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            {appointments && appointments.map(appointment => {
                const technician = technicians && technicians.find(tech => tech.id === appointment.technician_id);
                const automobile = automobiles && automobiles.find(auto => auto.id === appointment.automobile_id);
                const date = new Date(appointment.date_time);
                const formattedDate = date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
                const formattedTime = date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0');
                return (
                    <tr key={appointment.id}>
                        <td>{appointment.vin}</td>
                        <td>{filteredAutomobiles.some(
                                                (automobile) => automobile.vin === appointment.vin
                                            )
                                                ? 'Yes'
                                                : 'No'}</td>
                        <td>{appointment.customer}</td>
                        <td>{formattedDate + ' ' + formattedTime}</td>
                        <td>{technicians ? `${technician.first_name} ${technician.last_name}` : 'N/A'}</td>
                        <td>{appointment.reason}</td>
                        <td>{appointment.status}</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    </div>
);
}
export default ServiceHistory;