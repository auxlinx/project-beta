import React, { useEffect, useState } from 'react';

function ListAllServiceAppointments() {
    const [updateStatus, setUpdateStatus] = useState();
    const [appointments, setAppointments] = useState();
    const [automobiles, setAutomobiles] = useState();
    const [technicians, setTechnicians] = useState();
    const [error, setError] = useState();
    const [message, setMessage] = useState("");

    const getTechnitionData = async () => {
        // gets database information from docker server
        const techniciansurl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(techniciansurl);
    
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }  
      
        useEffect(() => {
            getAppointmentsData().then(data => setAppointments(data));
            getTechnitionData().then(data => setTechnicians(data));
            getTechnitionData();
        }, []);

    const getAppointmentsData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            } else {
                setError('Failed to fetch appointments data');
            }
        } catch (error) {
            setError('An error occurred while fetching appointments data');
        }
    };

    useEffect(() => {
        getAppointmentsData();
    }, []);


    const updateAppointmentStatusCancel = async (id) => {
    window.location.reload();
    const appointmentsUrl = `http://localhost:8080/api/appointments/${id}/`;
    const fetchConfig = {
    method: "PUT",
    body: JSON.stringify({ status: "Cancel" }),
    headers: {'Content-Type': 'application/json',},
    };
    const response = await fetch(appointmentsUrl, fetchConfig);
  } 

    useEffect(() => {
        setUpdateStatus();
    }, []);


    const updateAppointmentStatusFinished = async (id) => {
        window.location.reload();
        const appointmentsUrl = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
        method: "PUT",
        body: JSON.stringify({ status: "Finished" }),
        headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(appointmentsUrl, fetchConfig);
      } 
    
        useEffect(() => {
            setUpdateStatus();
        }, []);



    
    if (error) {
        return <div>Error: {error}</div>;
    }

    return (
        
        <div>
            <h1>Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Date Time</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Technician</th>
                        <th>VIP</th>
                        <th>Cancel Appointment</th>
                        <th>Appointment Finished</th>
                    </tr>
                </thead>
                <tbody>
                {appointments && appointments.filter(appointment => appointment.status === ('Active' || 'active')).map(appointment => {
                    const technician = technicians && technicians.find(tech => tech.id === appointment.technician_id);
                    const automobile = automobiles && automobiles.find(auto => auto.id === appointment.automobile_id);
                    const result = automobile && automobile.sold ? 'Yes' : 'No';

                    const date = new Date(appointment.date_time);
  const formattedDate = date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
  const formattedTime = date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes().toString().padStart(2, '0');
                    return (
                        <tr key={appointment.id}>
                            <td>{formattedDate + ' ' + formattedTime}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer}</td>
                            <td>{technicians ?  (technician.first_name + " " + technician.last_name) : 'N/A'}</td>
                            <td>{result}</td>
                            <td ><button className="btn-danger"  onClick={() => updateAppointmentStatusCancel(appointment.id)}>Cancel Appointment</button></td>
                            <td ><button className="btn-success" onClick={() => updateAppointmentStatusFinished(appointment.id)}>Appointment Finished</button></td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
}

export default ListAllServiceAppointments;
