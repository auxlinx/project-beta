import React, { useEffect, useState } from "react";
function ShowSale() {
    const [sales, setSales] = useState([]);
    const [selectedSale, setSelectedSale] = useState(null);


    const getSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl);
        if (response.ok) {
            const saleData = await response.json();
            console.log(saleData)

            if (saleData === undefined) {
                return null;
            }

            setSales(saleData.sales);
            console.log(saleData)
        } else {
            console.log('error');
        }
    }

    useEffect(() => {
        getSales();
    }, []);


    const handleSaleClick = (sale) => {
        setSelectedSale(sale);
    }


    const handleResetClick = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl);
        if (response.ok) {
            const saleData = await response.json();

            if (saleData === undefined) {
                return null;
            }
            setSales(saleData.sales);
            setSelectedSale(null);
        }
    }


    const handleDeleteClick = async () => {
        if (selectedSale) {
            const deleteUrl = `http://localhost:8090/api/sales/${selectedSale.id}`;
            const response = await fetch(deleteUrl, {
                method: 'DELETE'
            });
            const salesUrl = 'http://localhost:8090/api/sales/';
            const responseTwo = await fetch(salesUrl);
            if (response.ok) {
                const saleData = await responseTwo.json();


                setSales(saleData.sales);
                setSelectedSale(null);
            }
        }
    }


    return (
        <div>
            <h1 className="fw-bold shadow w-25 p-3 mb-5 bg-white rounded">Sales</h1>
            <table className="table table-hover">
                <thead>
                    <tr className="table-success">
                        <th className="fs-3" scope="col">Price</th>
                        <th className="fs-3" scope="col">Automobile VIN</th>
                        <th className="fs-3" scope="col">Salesperson</th>
                        <th className="fs-3" scope="col">Customer</th>
                    </tr>
                </thead>
                <tbody>
                    {sales && sales.map(sale => (
                        <tr
                            className={selectedSale === sale ? "table-primary" : ""}
                            key={sale.id}

                            onClick={() => handleSaleClick(sale)}
                        >
                            <td>${sale.price}</td>
                            <td>{sale.automobile}</td>
                            <td>{sale.salesperson}</td>
                            <td>{sale.customer}</td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {selectedSale && (
                <div>
                    <h2>Selected Sale Details:</h2>
                    <p>Price: ${selectedSale.price}</p>
                    <p>Automobile VIN: {selectedSale.automobile}</p>
                    <p>Salesperson: {selectedSale.salesperson}</p>
                    <p>Customer: {selectedSale.customer}</p>

                    <div/>
                    <div className="d-flex justify-content-start gap-2 ">

                    <button className="btn btn-danger " onClick={handleDeleteClick}>Delete Sale</button>
                    <button className="btn btn-primary" onClick={handleResetClick}>Reset</button>
                    </div>
                </div>
            )}
        </div>
    );
}

export default ShowSale;
