import React, { useEffect, useState } from "react";
function ShowModel() {
    const [models, setModels] = useState([]);
    const [selectedModel, setSelectedModel] = useState(null);


    const getModels = async () => {
        const modelsUrl = 'http://localhost:8100/api/models/';
        const response = await fetch(modelsUrl);
        if (response.ok) {
            const modelData = await response.json();

            if (modelData === undefined) {
                return null;
            }

            setModels(modelData.models);
            console.log(modelData)
        } else {
            console.log('error');
        }
    }

    useEffect(() => {
        getModels();
    }, []);


    const handleModelClick = (model) => {
        setSelectedModel(model);
    }


    const handleResetClick = async () => {
        const modelsUrl = 'http://localhost:8100/api/models/';
        const response = await fetch(modelsUrl);
        if (response.ok) {
            const modelData = await response.json();

            if (modelData === undefined) {
                return null;
            }
            setModels(modelData.models);
            setSelectedModel(null);
        }
    }


    const handleDeleteClick = async () => {
        if (selectedModel) {
            const deleteUrl = `http://localhost:8100/api/models/${selectedModel.id}/`;
            const response = await fetch(deleteUrl, {
                method: 'DELETE'
            });
            const modelsUrl = 'http://localhost:8100/api/models/';
            const responseTwo = await fetch(modelsUrl);
            if (response.ok) {
                const modelData = await responseTwo.json();


                setModels(modelData.models);
                setSelectedModel(null);
            }
        }
    }


    return (
        <div>
            <h1 className="fw-bold shadow w-25 p-3 mb-5 bg-white rounded">Models</h1>
            <table className="table table-hover">
                <thead>
                    <tr className="table-success">
                        <th className="fs-3" scope="col">Name</th>
                        <th className="fs-3" scope="col">Manufacturer</th>
                        <th className="fs-3" scope="col">Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models && models.map(model => (
                        <tr
                            className={selectedModel === model ? "table-primary" : ""}
                            key={model.id}

                            onClick={() => handleModelClick(model)}
                        >
                            <td className ="fw-bold">{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td><img className="list-image" src={model.picture_url} alt=""/></td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {selectedModel && (
                <div>
                    <h2>Selected Model Details:</h2>
                    <p>Name: {selectedModel.name}</p>
                    <p>Manufacturer: {selectedModel.manufacturer.name}</p>
                    <img className="list-image" src={selectedModel.picture_url} alt=""/>

                    <div/>
                    <div className="d-flex justify-content-start gap-2 ">

                    <button className="btn btn-danger " onClick={handleDeleteClick}>Delete Model</button>
                    <button className="btn btn-primary" onClick={handleResetClick}>Reset</button>


                    </div>
                </div>
            )}
        </div>
    );
}

export default ShowModel;
