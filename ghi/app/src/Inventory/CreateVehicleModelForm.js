import React, {useState, useEffect} from 'react';;

// this data if from the model encorder for Model
const initialData = {
    name: "", 
    picture_url: "",
    manufacturer_id: "",
}

function CreateVehicleModelForm() {
const [createdvehiclemodel, setCreatedVehicleModel] = useState();
// const [vehiclemodels, setVehicleModels] = useState();
const [manufacturers, setManufacturer] = useState();
const [formData, setFormData] = useState(initialData);
const [errors, setErrors] = useState({});
const [message, setMessage] = useState("");

const getManufacturers = async () => {
  // gets database information from docker server
  const manufacturemodelsurl = 'http://localhost:8100/api/manufacturers/';
  const response = await fetch(manufacturemodelsurl);
  if (response.ok) {
    const data = await response.json();
    setManufacturer(data.manufacturers);
  }
}  
useEffect(() => {
  getManufacturers();
}, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const newErrors = {};
    if (!formData.name) newErrors.name = 'name is required';
    if (!formData.picture_url) newErrors.picture_url = 'picture_url is required';
    if (!formData.manufacturer_id) newErrors.manufacturer_id = 'manufacturer is required';      

    if (Object.keys(newErrors).length > 0) {
        setErrors(newErrors);
        return;
      }

  const vehiclemodelsurl = 'http://localhost:8100/api/models/';

  const fetchConfig = {
    method: "post",
    body: JSON.stringify(formData),
    headers: {
      'Content-Type': 'application/json',
    },
  };

    const response = await fetch(vehiclemodelsurl, fetchConfig);

    if (response.ok) {
        setFormData(initialData)
        setCreatedVehicleModel(true);
      }
      // Set the message after form is submitted
      setMessage("Form submitted successfully!");
      // Clear the form fields
      setFormData(initialData);  
  } 

  const handleFormChange = (event) => {
    setFormData ({...formData, 
      [event.target.name]: event.target.value
    })
  }

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Model</h1>
            <form onSubmit={handleSubmit} id="create-Model-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.picture_url} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Model URL</label>
              </div>
              <div className="form-floating mb-3">
                    <select onChange={handleFormChange} value={formData.manufacturer_id} placeholder="manufacturer_id" required type="text" name="manufacturer_id" id="manufacturer_id" className="form-control" >
                    <option value="">Select Manufacturer</option>
                    {manufacturers && manufacturers.map(manufacturer => {
                      return (<option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>)
                    })}
                      </select>
                      <label htmlFor="manufacturer_id">Manufacturer</label>
              </div>
              <button className="btn btn-primary">Create Model</button>
            </form>
            {message && <p>{message}</p>}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateVehicleModelForm;