# CarCar
​
Team:
​
* Richard A Hofmann-Warner - Services
* Trevor Moore - Sales
​
## Design
​
**Instructions on how to run application:**
​
```
​
1. Go to remote repository using this link: https://gitlab.com/auxlinx/project-beta/
2. Click on blue 'Code' dropdown button
3. Click on clipboard icon that copies the url corresponding to 'Clone with HTTPS'
4. Open up a terminal window and run the command: cd into whatever directory you would like to clone this project into
5. Once in that directory, run the command: git clone https://gitlab.com/auxlinx/project-beta.git
6. Run the command: ls (to see what the name of the cloend project is)'
7. Run the command: cd in to that directory
8. Run the command: docker-compose up to get all of the necessary docker containers up and running
9. Type localhost:3000 into your browser to access the application
10. Explore!
​
```
​
**Diagram of Bounded Contexts:**

- CarCar Management Microservice Design
![Img](/Project-BetaDiagram.png)

​
```
​
**High level design:**
​
```
​
CarCar is made up of 1 React application and 3 microservices (bounded contexts) interacting with each other and a postgres database for each:
​
-*React application*
- *Inventory*
- *Services*
- *Sales*
​
```
​
## Sales microservice
​​
Trevor:
​
**Models Description:**
​
```
​
I created the models inside of the Sales microservice. Inside the models folder for sales there are four models: Sale, Salesperson, Customer and AutomobileVO.
​
The Customer model is configured in such a way to ensure that when an object is created the database is tracking the following fields:
- *first_name*
- *last_name*
- *address*
- *phone_number*

The Salesperson model is configured in such a way to ensure that when an object is created, the database is tracking the following fields:
- *first_name*
- *last_name*
- *employee_id*
- *picture_url*

The Sale model is configured in such a way to ensure that when an object is created, the database is tracking the following fields:
- *price*
- *automobile* - This is a foreign key that connects to the following AutomobileVO model to poll data from the inventory microservice for Automobiles.
- *salesperson* - This is a foreign key used to get data from the above Salesperson model.
- *customer* - This is a foreign key used to get data from the above Customer model.


​
The AutomobileVO model is a value object designed so that for each Automobile object added to the database through the Automobile model entity defined in the Inventory microservice, a copy of that Automobile object is created that tracks only the properties from the Automobile model we define in the value object.
- *import_href*
- *vin*
- *sold*
​
There is a poller in operation on the sales microservice that is facilitating the retrieval of information from the Inventory microservice. For the purpose of the Sales microservice the poller is pulling the Automobile information and returning it so that it may be accessed and attached to the above AutomobileVO model.
​
```
​
**API Endpoints**
​
```
| Action | Method | URL
| ---------- | ---------- | ----------
| List Customers | GET | http://localhost:8090/api/customers/
------------------------------------------------------
| Create a Customer | POST | http://localhost:8090/api/customers/
------------------------------------------------------
| Delete a specific Customer | DELETE | http://localhost:8090/api/customers/id/
------------------------------------------------------
| Update a specific Customer | PUT | http://localhost:8090/api/customers/id/
------------------------------------------------------
| List Salespeople | GET | http://localhost:8090/api/salespeople/
------------------------------------------------------
| Create a Salesperson | POST | http://localhost:8090/api/salespeople/
------------------------------------------------------
| Delete a specific Salesperson | DELETE | http://localhost:8090/api/salespeople/id/
------------------------------------------------------
| List Sales | GET | http://localhost:8090/api/sales/
------------------------------------------------------
| Create a Sale | POST | http://localhost:8090/api/sales/
------------------------------------------------------
| Delete a specific Sale | DELETE | http://localhost:8090/api/sales/id/
------------------------------------------------------

```
​
*When requesting the list of cusotmers in the database, the JSON response will look like this:*
```
{
	"customers": [
		{
			"first_name": "Danny",
			"last_name": "Boone",
			"address": "111 galvanize dr. gop, KY",
			"phone_number": "5555555555",
			"id": 1
		}
	]
}
​
```
​
```
*To create a new customer, send a JSON body following this format:*
```

{
	"first_name": "Danny",
	"last_name": "Boone",
	"address": "111 galvanize dr. gop, KY",
	"phone_number": "5555555555"
}
​
```
​
*When deleting a specific customer, the JSON response will look like this (if the designated customer does not exist in the database, the value for "deleted" will be false):*
```
{
    "deleted": true
}
​
```
*When requesting the list of salespeople in the database, the JSON response will look like this:*
```
{
	"salespeople": [
		{
			"first_name": "Trevor",
			"last_name": "Moore",
			"employee_id": "123TM",
			"id": 1,
			"picture_url": ""
		},
		{
			"first_name": "B.",
			"last_name": "W.",
			"employee_id": "321BW",
			"id": 2,
			"picture_url": ""
		}
	]
}
*To create a new salesperson, send a JSON body following this format:*
```

{
	"first_name": "Trevor",
	"last_name": "Moore",
	"employee_id": "1"
}

```
*When deleting a specific salesperson, the JSON response will look like this (if the designated salesperson does not exist in the database, the value for "deleted" will be false):*
```
{
    "deleted": true
}

```
*When requesting the list of sales in the database, the JSON response will look like this:*
```
{
	"sales": [
		{
			"price": 59.99,
			"automobile": "1C3CC5FB2AN120177",
			"salesperson": "B. W.",
			"customer": "Danny Boone",
			"id": 2,
			"salesperson_employee_id": "321BW"
		},
		{
			"price": 199.99,
			"automobile": "1C3CC5FB2AN120172",
			"salesperson": "Trevor Moore",
			"customer": "Danny Boone",
			"id": 3,
			"salesperson_employee_id": "123TM"
		}
	]
}
```
*To create a new sale, send a JSON body following this format:*
```

{
	"price": 30000.99,
	"automobile": "/api/automobiles/1C3CC5FB2AN120174/",
	"salesperson": 1,
	"customer": 1
}
```
*When deleting a specific sale, the JSON response will look like this (if the designated sale does not exist in the database, the value for "deleted" will be false):*
```
{
    "deleted": true
}
```

​
## Services microservice
​
Richard Hofmann-Warner:
​
**Models Description:**
​
```
​
I created the models inside of the Services microservice. Inside the models folder for services there are three models: Technician, Appointment, and AutomobileVO.
​
The Technician model is configured in such a way to ensure that when an object is created the database is tracking the following fields:
- *first_name*
- *last_name*
- *employee_id*


The Appointment model is configured in such a way to ensure that when an object is created, the database is tracking the following fields:
-   status - This defaults to active
    *STATUS_CHOICES*
    ('cancel', 'Cancelled')
    ('finish', 'Finished')
    ('active', 'Active')
- date_time
- reason
- vin
-customer
- *technician* - This is a foreign key used to get data from the above Technician model.
​
The AutomobileVO model is a value object designed so that for each Automobile object added to the database through the Automobile model entity defined in the Inventory microservice, a copy of that Automobile object is created that tracks only the properties from the Automobile model we define in the value object.
- *import_href*
- *vin*
- *sold*
​
There is a poller in operation on the services microservice that is facilitating the retrieval of information from the Inventory microservice. For the purpose of the Services microservice the poller is pulling the Automobile information and returning it so that it may be accessed and attached to the above AutomobileVO model.
​
```
​
**API Endpoints**
​
```
| Action | Method | URL
| ---------- | ---------- | ----------
| List Technicans | GET | http://localhost:8080/api/technicians/
------------------------------------------------------
| Create a Technican | POST | http://localhost:8080/api/technicians/
------------------------------------------------------
| Delete a specific Technican | DELETE | http://localhost:8080/api/technicians/id/
------------------------------------------------------
| Update a specific Technican | PUT | http://localhost:8080/api/technicians/id/
------------------------------------------------------
| List Appointments | GET | http://localhost:8080/api/appointments/
------------------------------------------------------
| Create a Appointment | POST | http://localhost:8080/api/appointments/
------------------------------------------------------
| Delete a specific Appointment | DELETE | 	http://localhost:8080/api/appointments/id/
------------------------------------------------------
| Update a specific Appointment | PUT | http://localhost:8080/api/appointments/id/
------------------------------------------------------
| Create a Appointment | POST | http://localhost:8090/api/services/
------------------------------------------------------
| Delete a specific Appointment | DELETE | http://localhost:8090/api/services/id/
------------------------------------------------------

```
​
*When requesting the list of technicians in the database, the JSON response will look like this:*
```
{
	{
			"id": 4,
			"first_name": "Rich",
			"last_name": "Tims",
			"employee_id": 5
		}
}
​
```
​
```
*To create a new technican, send a JSON body following this format:*
```

{
	"first_name": "Timmy",
  	"last_name": "Tester",
  	"employee_id": 7
}
​
```
​
*When deleting a specific technican, the JSON response will look like this (if the designated technican does not exist in the database, the value for "deleted" will be false):*
```
{
  "last_name": "Anderson"
	
}
​
```
*When requesting the list of technician in the database, the JSON response will look like this:*
```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Bill",
			"last_name": "Clinton",
			"employee_id": 1
		},
		{
			"id": 3,
			"first_name": "Timmy",
			"last_name": "tester",
			"employee_id": 3
		},
		{
			"id": 4,
			"first_name": "Rich",
			"last_name": "Tims",
			"employee_id": 5
		}
    ]
}

*When requesting the list of appointments in the database, the JSON response will look like this:*
```
{
	"appointments": [
		{
			"id": 5,
			"date_time": "2024-05-12T00:00:00+00:00",
			"reason": "Bumper test",
			"status": "Active",
			"vin": "trs99gf0tr",
			"customer": "Tammy Jones",
			"technician_id": 1
		},
		{
			"id": 6,
			"date_time": "2024-03-12T00:00:00+00:00",
			"reason": "Oil Change",
			"status": "Cancel",
			"vin": "245539RT24R",
			"customer": "Jennifer Davis",
			"technician_id": 2
		}
    ]
}
```
*To create a new appointment, send a JSON body following this format:*
```

{
	"date_time": "2024-01-12",
  "reason": "Brake fluid Change",
  "status": "Active",
  "vin": "srt484ft4r55t",
  "customer": "James Davis", 
  "technician": "5"
}
```
*When deleting a specific appointment, the JSON response will look like this (if the designated appointment does not exist in the database, the value for "deleted" will be false):*
```
{
    "deleted": true
}
```
