from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(null=True, max_length=200, unique=True)
    vin = models.CharField(max_length=100)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.IntegerField()

    def __str__(self):
        return self.first_name + " " + self.last_name


class Appointment(models.Model):
    STATUS_CHOICES = [
        ('cancel', 'Cancelled'),
        ('finish', 'Finished'),
        ('active', 'Active'),
    ]
    date_time = models.DateTimeField(null=True)
    reason = models.CharField(null=True, max_length=200)
    status = models.CharField(
        null=True,
        max_length=10,
        choices=STATUS_CHOICES,
        default='active',
    )
    vin = models.CharField(null=True, max_length=200)
    customer = models.CharField(null=True, max_length=200)
    technician = models.ForeignKey(
        Technician,
        null=True,
        related_name="Technicians",
        on_delete=models.CASCADE
    )
